using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PortalHAndler : MonoBehaviour
{
    public ParticleSystem system;
    public GameObject outPortal;
    public GameObject[] characters;

    private bool teleportEnabled;
    //This is Main Camera in the Scene
    Camera m_MainCamera;

    // Start is called before the first frame update
    void Start()
    {
        //This gets the Main Camera from the Scene
        m_MainCamera = Camera.main;
        teleportEnabled = false;
    }

    /*void OnCollisionEnter(Collision collision)
    {
        system.Play(true);
    }*/

    void OnTriggerEnter(Collider other)
    {
        if (teleportEnabled)
        {
            system.Play(true);
            Vector3 newPosition = outPortal.transform.position;

            characters = GameObject.FindGameObjectsWithTag("Character");

            //move characters
            float i = 0;
            float step = (float)2.0 / characters.Length;
            foreach (GameObject character in characters)
            {
                i = i + step;

                Vector3 newCharacterPosition = new Vector3(newPosition.x + (Mathf.Sin(Mathf.PI * i) * 4), newPosition.y, newPosition.z + (Mathf.Cos(Mathf.PI * i) * 4));
                Debug.Log(Mathf.PI * i);
                Debug.Log(i);

                Debug.Log(Mathf.Sin(Mathf.PI * i));
                Debug.Log(Mathf.Cos(Mathf.PI * i));
                character.transform.position = newCharacterPosition;

                character.GetComponent<UnityEngine.AI.NavMeshAgent>().SetDestination(newCharacterPosition);
                /*Character charScript = character.FindObjectOfType(typeof(Character)) as Character;
                charScript.SetDestination(newCharacterPosition);*/

                ParticleSystem[] childs = character.GetComponentsInChildren<ParticleSystem>();
                foreach (ParticleSystem particle in childs)
                {
                    particle.Play(true);
                }
            }

            //move camera
            Vector3 newCameraPosition = new Vector3(newPosition.x, 12, newPosition.z - 12);
            GameObject cameraObject = GameObject.Find("CameraSystem");
            cameraObject.transform.position = newCameraPosition;

            teleportEnabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
            teleportEnabled = true;
    }
}

