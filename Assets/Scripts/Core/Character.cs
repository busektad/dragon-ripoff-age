using UnityEngine;

namespace Core
{
public class Character : MonoBehaviour
{
    
    [field: SerializeField, Header("General")]
    public int Index { get; private set; }

    private GameObject _selectionRing;
    private Mover _mover;
    
    public void SetDestination(Vector3 dest)
    {
        _mover.SetDestination(dest);
    }

    public void OnSelect()
    {
        _selectionRing.SetActive(true);
    }

    public void OnDeselect()
    {
        _selectionRing.SetActive(false);
    }

    
    private void Awake()
    {
        _selectionRing = transform.GetChild(0).gameObject;
        _mover = GetComponent<Mover>();
    }
}

}