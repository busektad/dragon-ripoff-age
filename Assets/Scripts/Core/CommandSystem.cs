using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Core
{
public class CommandSystem : MonoBehaviour
{
    [SerializeField] public SelectionSystem selectionSystem;

    // standard binding to right click
    public void CommandAction(InputAction.CallbackContext context)
    {
        if (!context.performed) return;
        
        var selectedCharacters = selectionSystem.SelectedCharacters;
        if (selectedCharacters.Count == 0) return;

        if (!Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out var hit, 100)) return;
        
        CommandMove(selectedCharacters, hit.point);

    }

    private static void CommandMove(List<Character> selectedCharacters, Vector3 dest)
    {
        foreach (var character in selectedCharacters)
        {
            character.SetDestination(dest);
        }
    }

}
}
