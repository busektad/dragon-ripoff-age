using UnityEngine;
using UnityEngine.AI;

namespace Core
{
public class Mover : MonoBehaviour
{

    [SerializeField] public float moveThreshold = 0.1f;
    [SerializeField] public float maxAnimationSpeed = 3f;
    
    private NavMeshAgent _agent;
    private Animator _animator;
    private static readonly int Moving = Animator.StringToHash("moving");
    private static readonly int ForwardSpeed = Animator.StringToHash("forwardSpeed");

    private float _scale;

    public void SetDestination(Vector3 dest)
    {
        _agent.destination = dest;
    }
    
    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _animator.applyRootMotion = false;
        _scale = transform.GetChild(1).transform.localScale.x;
    }
    
    private void Update()
    {
        Vector3 localVelocity = transform.InverseTransformDirection(_agent.velocity);
        float velocity = localVelocity.magnitude / _scale;
        
        bool isMoving = velocity > moveThreshold;
        _animator.SetBool(Moving, isMoving);
        _animator.SetFloat(ForwardSpeed, velocity);
        float animSpeed = Mathf.Max(1f, velocity / maxAnimationSpeed);
        _animator.speed = animSpeed;
    }
}
}
