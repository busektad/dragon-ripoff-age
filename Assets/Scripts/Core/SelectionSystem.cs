using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

namespace Core
{
public class SelectionSystem : MonoBehaviour
{
    private const int NumCharacters = 4;

    [SerializeField] public Character[] characters = new Character[NumCharacters];

    private readonly Button[] _portraits = new Button[NumCharacters];

    private VisualElement _selectionRect;

    private List<int> _selectedCharacters;
    
    private bool _clickHold;

    private Vector2 _clickOriginPos = Vector2.zero;

    public List<Character> SelectedCharacters => _selectedCharacters.Select(i => characters[i]).ToList();
    
    // standard binding to left click
    public void SelectAction(InputAction.CallbackContext context)
    {
        if (context.started) ClickStart();
        if (context.canceled) ClickEnd();
    }
    
    // standard binding to "C"
    public void SelectAllAction(InputAction.CallbackContext context)
    {
        if (context.performed)
            SelectAll();
    }
    
    private static Vector2 MouseToScreen() => new(Input.mousePosition.x, Screen.height - Input.mousePosition.y);

    private void Awake()
    {
        var ui = GameObject.Find("UIDocument").GetComponent<UIDocument>().rootVisualElement;

        for (int i = 0; i < NumCharacters; i++)
        {
            _portraits[i] = ui.Q<Button>("BtnCharacter" + i);
            var localIndex = i;
            _portraits[i].clicked += () => Select(localIndex);
        }

        ui.Q<Button>("BtnSelectAll").clicked += SelectAll;
        _selectionRect = ui.Q<VisualElement>("SelectionRect");
        
        _selectedCharacters = new List<int>();
    }

    private void ClickStart()
    {
        _clickHold = true;
        _clickOriginPos = MouseToScreen();
        _selectionRect.style.visibility = Visibility.Visible;
    }

    private void ClickEnd()
    {
        _clickHold = false;
        _selectionRect.style.visibility = Visibility.Hidden;
        var pos = MouseToScreen();

        if (Mathf.Abs(pos.x - _clickOriginPos.x) > 0 || Mathf.Abs(pos.y - _clickOriginPos.y) > 0)
        {
            var charsToSelect = VolumeSelection(pos);
            if (charsToSelect.Count == 0) return;

            Select(charsToSelect);
        }

        else
        {
            var charToSelect = PointSelection();
            if (charToSelect < 0) return;

            Select(charToSelect);
        }
    }

    private int PointSelection()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit, Mathf.Infinity, LayerMask.GetMask("Entity"))) return -1;
        if (!hit.collider.CompareTag("Character")) return -1;

        return hit.transform.gameObject.GetComponent<Character>().Index;
    }

    private List<int> VolumeSelection(Vector2 clickEndPos)
    {
        var minX = Mathf.Min(_clickOriginPos.x, clickEndPos.x);
        var maxX = Mathf.Max(_clickOriginPos.x, clickEndPos.x);
        var minY = Mathf.Min(_clickOriginPos.y, clickEndPos.y);
        var maxY = Mathf.Max(_clickOriginPos.y, clickEndPos.y);

        var charIndices = new List<int>();
        foreach (var character in characters)
        {
            var p = Camera.main.WorldToScreenPoint(character.transform.position);
            p.y = Screen.height - p.y;
            if (p.x > minX && p.x < maxX && p.y > minY && p.y < maxY) charIndices.Add(character.Index);
        }

        return charIndices;
    }

    private void Select(int charIndex)
    {
        Select(new List<int> { charIndex });
    }

    private void Select(List<int> charIndices)
    {
        foreach (int i in _selectedCharacters)
        {
            characters[i].OnDeselect();
            DeactivatePortrait(i);
        }

        foreach (int i in charIndices)
        {
            characters[i].OnSelect();
            ActivatePortrait(i);
        }

        _selectedCharacters = charIndices;
    }

    private void SelectAll()
    {
        Select(Enumerable.Range(0, NumCharacters).ToList());
    }

    private void ActivatePortrait(int index)
    {
        _portraits[index].style.borderBottomWidth = 3f;
        _portraits[index].style.borderTopWidth = 3f;
        _portraits[index].style.borderLeftWidth = 3f;
        _portraits[index].style.borderRightWidth = 3f;
    }

    private void DeactivatePortrait(int index)
    {
        _portraits[index].style.borderBottomWidth = 0f;
        _portraits[index].style.borderTopWidth = 0f;
        _portraits[index].style.borderLeftWidth = 0f;
        _portraits[index].style.borderRightWidth = 0f;
    }

    private void UpdateSelectionRectangle()
    {
        var currentPos = MouseToScreen();
        float width = Mathf.Abs(currentPos.x - _clickOriginPos.x);
        float height = Mathf.Abs(currentPos.y - _clickOriginPos.y);
        float left = Mathf.Min(currentPos.x, _clickOriginPos.x);
        float top = Mathf.Min(currentPos.y, _clickOriginPos.y);
        _selectionRect.style.width = width;
        _selectionRect.style.height = height;
        _selectionRect.style.left = left;
        _selectionRect.style.top = top;
    }

    private void Update()
    {
        if (!_clickHold) return;

        UpdateSelectionRectangle();
    }
}
}