using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;


public class CameraSystem : MonoBehaviour
{
    private Transform _transform;
    private bool _isMoved;
    private bool _isRotated;
    private bool _isEdgeScrolled;
    private Vector3 _wasdDir;
    private Vector3 _edgeScrollDir;
    private float _rotateDir;
    private float _targetFOV;

    [SerializeField] public CinemachineVirtualCamera virtualCamera;
    [Header("Camera Settings")] 
    public float moveSpeed;

    public float edgeScrollSpeed;

    public float rotateSpeed;

    public int zoomSpeed;

    public float zoomLerpFactor;

    public int edgeScrollPixelThreshold;

    public int minFOV;

    public int maxFOV;

    public bool useEdgeScrolling;
    
    private void Start()
    {
        _transform = transform;
        _targetFOV = virtualCamera.m_Lens.FieldOfView;
    }
    
    private void Update()
    {
        if (useEdgeScrolling)
            EdgeScrolling();

        if (_isMoved || _isEdgeScrolled)
        {
            Vector3 inputDir = _wasdDir + _edgeScrollDir;
            var moveDir = _transform.forward * inputDir.z + _transform.right * inputDir.x;
            _transform.position += Time.deltaTime * moveDir;
        }

        if (_isRotated)
        {
            _transform.eulerAngles += new Vector3(0, _rotateDir * rotateSpeed * Time.deltaTime, 0);
        }
        
        virtualCamera.m_Lens.FieldOfView =
            Mathf.Lerp(virtualCamera.m_Lens.FieldOfView, _targetFOV, zoomLerpFactor * Time.deltaTime);
    }

    // standard binding to "WASD"
    public void CameraMovement(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            Vector2 vec2 = context.ReadValue<Vector2>();
            _wasdDir = new Vector3(vec2.x * moveSpeed, 0, vec2.y * moveSpeed);
            _isMoved = true;
        }
        
        else if (context.canceled)
        {
            _wasdDir = new Vector3(0, 0, 0);
            _isMoved = false;
        }
    }

    // standard binding to "QE"
    public void CameraRotation(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            _rotateDir = context.ReadValue<float>();
            _isRotated = true;
        }

        else if (context.canceled)
        {
            _isRotated = false;
        }
    }

    // standard binding to mouse wheel
    public void CameraZoom(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            float dir = context.ReadValue<float>();
            _targetFOV = Mathf.Clamp(_targetFOV + zoomSpeed * dir, minFOV, maxFOV);
        }
    }

    private void EdgeScrolling()
    {
        _edgeScrollDir = new Vector3(0, 0, 0);
        _isEdgeScrolled = false;

        // up
        if (Input.mousePosition.y > Screen.height - edgeScrollPixelThreshold)
        {
            _edgeScrollDir.z = 1f * edgeScrollSpeed;
            _isEdgeScrolled = true;
        }

        // down
        if (Input.mousePosition.y < edgeScrollPixelThreshold)
        {
            _edgeScrollDir.z = -1f * edgeScrollSpeed;
            _isEdgeScrolled = true;
        }
        
        // left
        if (Input.mousePosition.x < edgeScrollPixelThreshold)
        {
            _edgeScrollDir.x = -1f * edgeScrollSpeed;
            _isEdgeScrolled = true;
        }
        
        // right
        if (Input.mousePosition.x > Screen.width - edgeScrollPixelThreshold)
        {
            _edgeScrollDir.x = 1f * edgeScrollSpeed;
            _isEdgeScrolled = true;
        }
    }
}
